import React from 'react';
import './App.css';
import Sockette from "sockette";

function App() {

  const [text, setText] = React.useState<string>("");
  const [socket, setSocket] = React.useState<Sockette>(null!);
  const [messages, setMessages] = React.useState<string[]>([]);

  const connect = () => {
    const ws = new Sockette("", {
      maxAttempts: 5,
      onmessage: handleMessage
    })
    setSocket(ws);
  }

  const disconnect = () => {
    socket.close();
  }

  const handleMessage = (e: any) => {
    console.log(JSON.stringify(e));
    setMessages([...messages, JSON.stringify(e)]);
  }

  const send = () => {
    socket.json({ action: "onMessage", data: text });
  }

  return (
    <div>
      <div>
        <button onClick={connect}>Connect</button>
        <button onClick={disconnect}>Leave</button>
      </div>
      <div>
        <textarea style={{ width: "400px", height: "300px" }} value={messages} />
      </div>
      <div>
        <input type="text" value={text} onChange={(e) => setText(e.target.value)} />
        <button onClick={send}>Send</button>
      </div>
    </div>
  );
}

export default App;
