import AWS from "aws-sdk";
import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";

export const handleConnect = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    if (event.requestContext.connectionId) {
        await addConnectionRecord("test", event.requestContext.connectionId);
        return ok();
    }
    return badRequest();
}

export const handleDisconnect = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    if (event.requestContext.connectionId) {
        await deleteConnectionRecord("test", event.requestContext.connectionId);
        return ok();
    }

    return badRequest();
}

export const handleMessage = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {

    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        apiVersion: "2018-11-29",
        endpoint: event.requestContext.domainName + "/" + event.requestContext.stage
    });

    const message = JSON.parse(event.body!).data;
    const connections = await getConnections("test");
    const senders = connections.map(conn => {
        return apigwManagementApi.postToConnection({ ConnectionId: conn, Data: message }).promise()
    });

    try {
        await Promise.all(senders);
    } catch (err) {
        console.log(err);
        return badRequest();
    }

    return ok();
}

const ok = (body?: any): APIGatewayProxyResult => {
    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Credentials": true
        },
        body: body ? JSON.stringify(body) : ""
    };
};

const badRequest = (): APIGatewayProxyResult => {
    return {
        statusCode: 400,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Credentials": true
        },
        body: ""
    };
}

let dynamoConfig: any = {
    apiVersion: "2012-08-10",
    region: "eu-west-2"
};

const dynamoDB = new AWS.DynamoDB(dynamoConfig);
const docClient = new AWS.DynamoDB.DocumentClient(dynamoConfig);
const defaultTennantId = "tennant:default";
const tableName = process.env.TABLE_NAME!;

export const addConnectionRecord = async (sessionId: string, connectionId: string): Promise<void> => {
    const params: AWS.DynamoDB.PutItemInput = {
        TableName: tableName,
        Item: {
            TennantId: { S: defaultTennantId },
            RecordId: { S: `session:${sessionId}:${connectionId}` }
        }
    };

    await dynamoDB.putItem(params).promise();
};

export const deleteConnectionRecord = async (sessionId: string, connectionId: string): Promise<void> => {
    const params: AWS.DynamoDB.DeleteItemInput = {
        TableName: tableName,
        Key: {
            TennantId: { S: defaultTennantId },
            RecordId: { S: `session:${sessionId}:${connectionId}` }
        }
    }

    await dynamoDB.deleteItem(params).promise();
}

export const getConnections = async (sessionId: string): Promise<string[]> => {
    const params: AWS.DynamoDB.DocumentClient.QueryInput = {
        TableName: tableName,
        KeyConditionExpression: "TennantId = :tid and begins_with(RecordId, :ss)",
        ExpressionAttributeValues: {
            ":tid": defaultTennantId,
            ":ss": `session:${sessionId}:`
        }
    }

    const results = await docClient.query(params).promise();
    if (results.Items) {
        return results.Items.map(i => i.RecordId.split(":")[2])
    }

    return [];
}